/*
 * Copyright 2024 Hochschule Luzern Informatik.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.hslu.jacocoreader;

import nl.altindag.console.ConsoleCaptor;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Disabled;

import org.junit.jupiter.api.Test;

/**
 * Testcases.
 */
class JaCoCoReaderTest {

    @Test
    void testNoArgument() {
        final ConsoleCaptor consoleCaptor = new ConsoleCaptor();
        JaCoCoReader.main(new String[0]);
        assertThat(consoleCaptor.getErrorOutput().getFirst()).contains("Argument mit Dateipfad fehlt");
    }

    @Test
    @Disabled(value = "temporary broken")
    void testNotExistingPath() {
        final String[] parameter = {"not/existing/path"};
        final ConsoleCaptor consoleCaptor = new ConsoleCaptor();
        JaCoCoReader.main(parameter);
        assertThat(consoleCaptor.getErrorOutput().get(0)).contains("Datei nicht gefunden");
    }

    @Test
    void testMissingTotal() {
        JaCoCoReader reader = new JaCoCoReader("src/test/resources/testdata/index-non-total.html");
        assertThat(reader.getStatementCoverage()).isZero();
        assertThat(reader.getBranchCoverage()).isZero();
    }

    @Test
    void testCoverage38and25() {
        JaCoCoReader reader = new JaCoCoReader("src/test/resources/testdata/index-38.html");
        assertThat(reader.getStatementCoverage()).isEqualTo(38);
        assertThat(reader.getBranchCoverage()).isEqualTo(25);
    }
}
