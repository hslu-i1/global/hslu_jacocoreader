/*
 * Copyright 2024 Hochschule Luzern Informatik.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ch.hslu.jacocoreader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Liest aus einem JaCoCo-Report das Total der Coverage und gibt es auf der
 * Konsole aus.
 */
public class JaCoCoReader {

    private static final Logger LOG;
    private static final String COVERAGE_REGEX = "Total.*?([0-9]{1,3})%.*?([0-9]{1,3})%";

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%5$s %n");
        LOG = Logger.getLogger(JaCoCoReader.class.getName());
    }

    private int statementCoverage = 0;
    private int branchCoverage = 0;

    /**
     * Main-Methode.
     *
     * @param args Pfad auf Datei.
     */
    public static void main(final String[] args) {
        if (args.length > 0) {
            final JaCoCoReader reader = new JaCoCoReader(args[0]);
            LOG.log(Level.INFO,
                    "Total Coverage Statement: {0}% / Branch: {1}% - provided by JaCoCoReader", new Object[] {reader.getStatementCoverage(), reader.getBranchCoverage()});
        } else {
            LOG.log(Level.WARNING, "JaCoCo-Reader: Argument mit Dateipfad fehlt.");
        }
    }

    /**
     * Extrahiert aus dem Coverage-Report den Coveragewert.
     * @param reportPath Pfad (String) auf den JaCoCo HMTL-Report.
     */
    public JaCoCoReader(final String reportPath) {
        final Path path = Paths.get(reportPath);
        if (Files.exists(path)) {
            final String content;
            try {
                content = Files.readAllLines(path).get(0);
                final Pattern pattern = Pattern.compile(COVERAGE_REGEX);
                final Matcher matcher = pattern.matcher(content);
                if (matcher.find() && matcher.groupCount() == 2) {
                    final String statementCoverageString = content.substring(matcher.start(1), matcher.end(1));
                    this.statementCoverage = Integer.parseInt(statementCoverageString);
                    final String branchCoverageString = content.substring(matcher.start(2), matcher.end(2));
                    this.branchCoverage = Integer.parseInt(branchCoverageString);
                }
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "JaCoCo-Reader: Datei nicht lesbar: {0}", ex.getMessage());
            }
        } else {
            LOG.log(Level.WARNING, "JaCoCo-Reader: Datei nicht gefunden: {0}", path);
        }
    }

    /**
     * Liefert das Total der Statement-Coverage.
     * @return Statement-Coverage in Prozent.
     */
    public final int getStatementCoverage() {
        return this.statementCoverage;
    }

    /**
     * Liefert das Total der Branche-Coverage.
     * @return Branch-Coverage in Prozent.
     */
    public final int getBranchCoverage() {
        return this.branchCoverage;
    }
}
