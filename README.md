# JaCoCo Reader

## Zweck
Liest aus JaCoCo Report (HTML) den Coveragewert und gibt ihn auf Konsole aus.
Somit kann die Coverage von GitLab (z.B. für den Coverage-Badge) ausgewertet
werden.

## Distribution / Download
- Siehe Package Repository: [Version 1.0.0](https://gitlab.com/hslu-i1/global/hslu_jacocoreader/-/packages/6404326)

## Verwendung
- not yet
